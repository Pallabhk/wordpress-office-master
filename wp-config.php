<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'office_master');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z<VLb(x&9vGpeizRHU?W7N)M}Pe|DFPRJ&=O#0Mfpn)fnGlBAKy`u,HUnhX1o@EG');
define('SECURE_AUTH_KEY',  'lTQkfF9mni$C)n[y^s.wD3hF6J>*M`dxGk}!r%F@znZyNeWQ]YvUR03JQ@tUyLQS');
define('LOGGED_IN_KEY',    '$KF}I`s,=tvrA~hl{B27)_S)V)sa:s;JTQ~hYyf,kz<Br_%FV(/nHT4V<|LZ3_;p');
define('NONCE_KEY',        ': $sw7oFp yO$0^bF_K,8IEvUTIy^kkFonH5RAh4wk$N6osq>!fM))YbV$P~m!9=');
define('AUTH_SALT',        'N)yCTeFf#~#%76z>_SOsY/2=,{{:(Lp6 oS^j8z]^ g;sIlTObx=Li1):HOf>DM.');
define('SECURE_AUTH_SALT', 'b}bvm4jAAD9m1j?K^3CMZ3HI@mn#!v?b?ZgKtaP@iF3zL:>;y[cCTO<n!]hH9g;3');
define('LOGGED_IN_SALT',   'L@?-|v;6-l2JyL(,zqj3-gfbAX(kDcrOhiP!~N=k%B*Jalu+yOpDji3?nEPZ*uZv');
define('NONCE_SALT',       '}#46`cEc{=<7Qr?ha*LwO;$jU=ZQo,pNduEI*MH$8:_D~GQigIiHNIFL:hN-62Rg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_office_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
