<?php get_header();?>

<?php

    if (have_posts()) {
        the_post();
        $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full' );?>
        
        <div class="row container-kamn">  
        <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
        
    </div>
    <?php  }
?>
    <!-- Main Container -->

    <div id="banners"></div>
    <div class="container">
        <div class="row">
            <?php 
                        $office_post = null;
                        $office_post =new WP_Query(array(
                            'post_type'     =>'team',
                            'post_per_page' =>-1
                        ));

                        if ($office_post->have_posts()) {
                            while ($office_post->have_posts()) {
                                $office_post->the_post();
                                $block_quote =get_post_meta(get_the_ID(),'_office-master_block_quote',true);
                                $team_designation =get_post_meta(get_the_ID(),'_office-master_team_designation',true);
                                $team_animation =get_post_meta(get_the_ID(),'_office-master_team_animation',true);
                                ?>
                                    
                            <div class="col-md-6">
                                <div class="blockquote-box <?php echo $block_quote;?> animated wow <?php echo $team_animation;?> clearfix">
                                    <div class="square pull-left">
                                   <?php the_post_thumbnail('team-img');?>
                                    </div>
                                    <h4>
                                       <?php the_title();?>
                                    </h4>
                                    <p>
                                      <?php echo  $team_designation;?>
                                    </p>
                                </div>
                            </div>
                          
                

                            <?php }
                                    }else{
                                        echo "No post Here";
                                    }
                                   
                        ?>  
        </div>
    </div>
    <!--End Main Container -->


    <!-- Footer -->
 <?php get_footer();?>