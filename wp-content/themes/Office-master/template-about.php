<?php
/*
Template Name: For about Page
*/
 get_header();

 ?>

<?php

    if (have_posts()) {
        the_post();
        $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
        $content    =get_the_content();
         $group_meta_field =get_post_meta(get_the_ID(),'_office-master_groupable_field_meta_field',true);
         ?>
        
        <div class="row container-kamn">  
        <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
        
    </div>
    <?php  }
?>

    <div id="banners"></div>
    <div class="container">   
        <div class="row">
            <div class="side-left col-sm-4 col-md-4">
                <?php 
                    foreach ($group_meta_field as $single) {?>
                        
                <h3 class="lead"><?php echo $single['_office-master_heading_field'];?> </h3><hr>

                <p><?php echo $single['_office-master_description_field'];?></p>

                    <?php
                        if (is_array($single['_office-master_a_link'])) {
                        
                     foreach($single['_office-master_a_link'] as $key=>$single_a){?>
                <a href="<?php echo $single_a;?>"><?php echo $single['_office-master_a_link_title'][$key];?></a><br>
               
                <?php } }?>
                <br>
    
                <?php }?>
            </div>
            <div class="col-sm-8 col-md-8">
              <?php
                    echo $content;
              ?>
            </div>  
        </div>    
    </div>  

    <!--End Main Container -->

  <?php get_footer();?>