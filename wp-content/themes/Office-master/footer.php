<?php

    global $office_master;
?>
    <!-- Footer -->
    <footer> 
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3><i class="fa <?php echo $office_master['col_1_icon'];?>"></i> <?php echo $office_master['col_1_title'];?></h3>
                    <p class="footer-contact">
                        <?php echo $office_master['contact_add'];?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fa <?php echo $office_master['col_2_icon'];?>"></i> <?php echo $office_master['col_2_title'];?></h3>
                    <?php
                        if (is_array($office_master['col_2_links'])) {
                      
                        foreach ($office_master['col_2_links'] as  $value) { ?>
                          <p> <a href="<?php echo $value['url'];?>"><?php echo $value['title'];?></a></p> 
                    <?php    }  } 
                    ?>
                    
                </div>
              <div class="col-md-4">
                <h3><i class="fa <?php echo $office_master['col_3_icon'];?>"></i> <?php echo $office_master['col_3_title'];?></h3>
                <div id="social-icons">
                     <?php
                        if (is_array($office_master['col_3_icons'])) {
                      
                        foreach ($office_master['col_3_icons'] as  $value) { ?>

                        <a href="<?php echo $value['url'];?>" class="btn-group google-plus">
                        <i class="fa <?php echo $value['title'];?>"></i> </a>
                          
                    <?php    }  } 
                    ?>
                </div>
              </div>    
        </div>
      </div>
    </footer>

    
    <div class="copyright text center">
        <?php  echo $office_master['copy_text'];?>
    </div>

    
   
   
    
    <?php wp_footer();?>
  </body>
</html>
