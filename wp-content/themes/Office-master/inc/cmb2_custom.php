<?php
require_once __DIR__ . '/cmb2/init.php';


add_action( 'cmb2_admin_init', 'office_master_metaboxes' );

/**
 * Define the metabox and field configurations.
 */

function office_master_metaboxes(){

	$prefix ='_office-master_';

	/**
	 * Service Metabox  metabox
	 */
	$Service_item = new_cmb2_box( array(

		'id'            => 'service_metabox',
		'title'         => __( 'Service Metabox', 'office_master' ),
		'object_types'  => array( 'services' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true 

	) );
	$Service_item->add_field( array(
		'name'       => __( 'Service Icon', 'office_master' ),
		'desc'       => __( 'Write your service icon font awosome name', 'office_master' ),
		'id'         => $prefix .'service_icon',
		'type'       => 'text',
		'repeatable'  =>'true'
	) );

	$Service_item->add_field( array(
		'name'       => __( 'Service Description', 'office_master' ),
		'desc'       => __( 'Write your service Description', 'office_master' ),
		'id'         => $prefix .'service_description',
		'type'       => 'textarea'
	) );

	$Service_item->add_field( array(
		'name'       => __( 'Service Link url', 'office_master' ),
		'desc'       => __( 'Write your service  Link url', 'office_master' ),
		'id'         => $prefix .'service_link_url',
		'type'       => 'text'
	) );

	$Service_item->add_field( array(
		'name'       => __( 'Service Link Title', 'office_master' ),
		'desc'       => __( 'Write your service Service Link Title', 'office_master' ),
		'id'         => $prefix .'service_link_title',
		'type'       => 'text'
	) );

	

	$Service_item->add_field( array(
		'name'       => __( 'Service Animation', 'office_master' ),
		'desc'       => __( 'Write your service Service Animation', 'office_master' ),
		'id'         => $prefix .'service_animation',
		'type'       => 'text'
	) );

	/**
	 * Slider Metabox  metabox
	 */
	$Slider_item = new_cmb2_box( array(

		'id'            => 'slider_metabox',
		'title'         => __( 'Slider Metabox', 'office_master' ),
		'object_types'  => array( 'slider' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true 

	) );
	$Slider_item->add_field( array(
		'name'       => __( 'Slider Caption', 'office_master' ),
		'desc'       => __( 'Write your Slider Caption', 'office_master' ),
		'id'         => $prefix .'slider_caption',
		'type'       => 'text'
	) );

	$special_page_item = new_cmb2_box( array(

		'id'            => 'Page_metabox',
		'title'         => __( 'Page Metabox', 'office_master' ),
		'object_types'  => array( 'page' ), // Post type
		'show_on'       =>array(
			'key'       =>'id',
			'value'     =>'11'
		),
 		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true 

	) );
	$special_page_item->add_field( array(
		'name'       => __( 'Page Caption', 'office_master' ),
		'desc'       => __( 'Write your Special Page Caption', 'office_master' ),
		'id'         => $prefix .'page_caption',
		'type'       => 'text'
	) );

	/**
	 * Team Metabox 
	 */

	$team_member = new_cmb2_box( array(

		'id'            => 'team_metabox',
		'title'         => __( 'Team Metabox', 'office_master' ),
		'object_types'  => array( 'team' ), // Post type
 		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true 

	) );

	$team_member->add_field( array(
		'name'       => __( 'Team Member Designation', 'office_master' ),
		'desc'       => __( 'Write your Team Member Designation', 'office_master' ),
		'id'         => $prefix .'team_designation',
		'type'       => 'text'
	) );
	$team_member->add_field( array(
		'name'       => __( 'Blockquote', 'office_master' ),
		'desc'       => __( 'Write your Block-qute color', 'office_master' ),
		'id'         => $prefix .'block_quote',
		'type'       => 'text'
	) );
	$team_member->add_field( array(
		'name'       => __( 'Animation', 'office_master' ),
		'desc'       => __( 'Write your Animation class name', 'office_master' ),
		'id'         => $prefix .'animation_class',
		'type'       => 'text'
	) );
	/**
	 * Post Metabox 
	 */

	$post_metabox= new_cmb2_box( array(

		'id'            => 'post_metabox',
		'title'         => __( 'Post Metabox', 'office_master' ),
		'object_types'  => array( 'post' ), // Post type
 		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true 

	) );

	$post_metabox->add_field( array(
		'name'       => __( 'Post file type', 'office_master' ),
		'desc'       => __( 'Write your Post file type', 'office_master' ),
		'id'         => $prefix .'post_file_type',
		'type'       => 'text'
	) );
	/**
	 * About Page Metabox 
	 * Repeatable group
	 */

		$about_page_group = new_cmb2_box( array(

		'id'            => 'about_page_metabox',
		'title'         => __( 'About Metabox', 'office_master' ),
		'object_types'  => array( 'page' ), // Post type
 		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true ,
		'show_on'    => array(

				'key'  =>'id',
				'value'=> '8'
		)

	) );

	$about_page      =$about_page_group->add_field( array(
		'name'       => __( 'Groupable  Field', 'office_master' ),
		'id'         => $prefix .'groupable_field_meta_field',
		'type'       => 'group'
		
	) );
	$about_page_group->add_group_field( $about_page, array(
		'name'       => __( 'Heading', 'office_master' ),
		'id'         => $prefix .'heading_field',
		'type'       => 'text'
		
	) );
	$about_page_group->add_group_field( $about_page, array(
		'name'       => __( 'Description  ', 'office_master' ),
		'id'         => $prefix .'description_field',
		'type'       => 'textarea'
		
	) );
	$about_page_group->add_group_field( $about_page, array(
		'name'       => __( 'A link  ', 'office_master' ),
		'id'         => $prefix .'a_link',
		'type'       => 'text',
		'repeatable' => true
		
	) );
	$about_page_group->add_group_field( $about_page, array(
		'name'       => __( 'A link Title ', 'office_master' ),
		'id'         => $prefix .'a_link_title',
		'type'       => 'text',
		'repeatable' => true
		
	) );

}