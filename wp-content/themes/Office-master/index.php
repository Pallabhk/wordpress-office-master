<?php get_header();?>
    
                 <?php 
                        $office_post = null;
                        $office_post =new WP_Query(array(
                            'post_type'     =>'page',
                            'post_per_page' =>-1
                        ));

                        if ($office_post->have_posts()) {
                            while ($office_post->have_posts()) {
                                $office_post->the_post();
                                if (get_the_ID() == 11) {
                                    $post_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full' );?>

                         <div class="row container-kamn">
                        <img src="<?php echo $post_thumb[0];?>" class="blog-post" alt="Feature-img" align="right" width="100%"> 
                        </div>
                   <?php  }  


                 }
                 }else{
                    echo "No post Here";
                 }
                 wp_reset_postdata();
                                   
              ?>   

    <!-- End Header -->


    <!-- Main Container -->
    <div id="banners"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                 <?php 
                    
                        if (have_posts()) {
                            while (have_posts()) {
                                the_post();
                                $post_file_type =get_post_meta(get_the_ID(),'_office-master_post_file_type',true);
                                ?>
                            <div class="blog-post">
                                <h1 class="blog-title">
                                    <i class="fa <?php echo $post_file_type;?>"></i>
                                  <a href="<?php the_permalink();?>"><?php the_title();?></a>  
                                </h1>
                                <br>
                                <?php the_post_thumbnail('post-img');?>
                                <br>
                                <p>
                                    <?php the_excerpt();?>
                                </p>
                                    <div>
                                        <span class="badge">Posted <?php echo get_the_date('Y-m-d H:i:s');?></span>
                                        <div class="pull-right">
                                            <?php the_tags('<span class="label label-default">','</span><span class="label label-primary">','</span>');?>
                                        </div>         
                                    </div>
                            </div>
                         <hr>  
                            
                            <?php }

                            ############Pagination##########

                            the_posts_pagination(array(

                                'prev_text'  => '«',
                                'next_text'  => '»',
                                'mid_size'   => '3'
                            ));
                                    }else{
                                        echo "No post Here";
                                    }
                                   
                        ?>   
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Sign in </strong></h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="<?php echo site_url();?>/wp-login.php" method="post">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username or Email</label>
                                    <input type="text" name="log" class="form-control" style="border-radius:0px" id="exampleInputEmail1" placeholder="Enter username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password <a href="<?php echo site_url();?>/wp-login.php?action=lostpassword">(forgot password)</a></label>
                                    <input type="password" name="pwd" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <input type="hidden" name="redirect_to" value="<?php echo site_url();?>">
                                <button type="submit" class="btn btn-sm btn-default">Sign in</button>
                            </form>
                        </div>
                    </div>

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                       
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php 
                        $office_post = null;
                        $office_post =new WP_Query(array(
                            'post_type'     =>'slider',
                            'post_per_page' =>-1
                        ));

                        if ($office_post->have_posts()) {
                            $x=0;
                            while ($office_post->have_posts()) {
                                $x++;
                                $office_post->the_post();?>
                                    
                                 <div class="item <?php if($x==1){echo 'active';}?>">

                                    <?php the_post_thumbnail('sub_slide-img');?>
                                
                                </div>
                      
                        <!-- End Slide 1 -->

                            <?php }
                                    }else{
                                        echo "No post Here";
                                    }
                            wp_reset_postdata();       
                        ?>
                        </div>

                         <!-- Indicators -->
                        <ol class="carousel-indicators">
                             <?php
                            for ($i=0; $i<$x; $i++) { ?>
                               <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i;?>" class="<?php if($i==0){ echo 'active';}?>"></li>
                          <?php  }
                        ?> 
                        </ol>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
       
        <!--End Main Container -->


        <!-- Footer -->
 <?php get_footer();?>