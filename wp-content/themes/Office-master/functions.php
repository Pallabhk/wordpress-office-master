<?php
	function office_master_theme_support(){

		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_image_size('slide-img',1500,500,true);
		add_image_size('sub_slide-img',265,192,true);
		add_image_size('team-img',100,80,true);
		add_image_size('post-img',850,490,true);
		register_nav_menus(array(
			'primary_menu' => 'Primary Menu'

		));
	}

	add_action('after_setup_theme','office_master_theme_support');


	///Css fuction add 

	function office_css_js(){
		//google-font
		wp_enqueue_style('google-font-1','//fonts.googleapis.com/css?family=Open+Sans:400,300', 'null','v1.9.0','all');
		wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=PT+Sans', 'null','v1.9.0','all');
		wp_enqueue_style('google-font-3','//fonts.googleapis.com/css?family=Raleway', 'null','v1.9.0','all');

		//Css and Bootstrap
		wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css');
		wp_enqueue_style('font-awsome',get_template_directory_uri().'/assets/css/font-awesome.min.css');
		wp_enqueue_style('Animate-css',get_template_directory_uri().'/assets/css/animate.min.css');
		wp_enqueue_style('Theme-css',get_template_directory_uri().'/assets/css/style.css');
		//main css
		wp_enqueue_style('main-css',get_stylesheet_uri());

		//Js file

		wp_enqueue_script('jquery');
		wp_enqueue_script('Bootstrap-js',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','jquery','v1.34','true');
		wp_enqueue_script('Wow-js',get_template_directory_uri().'/js/wow.min.js','jquery','v1.34','true');
	}
	add_action('wp_enqueue_scripts','office_css_js');

	function footer_extra_script(){ ?>
	<script>
      new WOW().init();
    </script>
	<?php }

	add_action('wp_footer','footer_extra_script',30);

	function office_master_fallback_manu(){ ?>
	
		<ul class="nav navbar-nav pull-right">
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Blog</a>
                    </li>
                    <li>
                        <a href="#">Team</a>
                    </li>
                    <li>
                        <a href="#"><span>Contact</span></a>
                    </li>
        </ul>
	<?php }

	function office_master_custom_post(){
		register_post_type('slider',array(
			'labels' =>array(
				'name'       =>'office_slider',
				'menu_name'  =>'Slider menu',
				'all_items'  =>'Slider all item',
				'add_new'    =>'Add new Slide',
				'add_new_item'=>'Add new slide item'
			),
			'public'  =>true,
			'supports'=>array(
				'title','thumbnail','revisions','custom-fields','page-attributes'
			)
		));
		register_post_type('services',array(
			'labels' =>array(
				'name'       =>'office_Service',
				'menu_name'  =>'Service menu',
				'all_items'  =>'Service all item',
				'add_new'    =>'Add new Service',
				'add_new_item'=>'Add new Service item'
			),
			'public'  =>true,
			'supports'=>array(
				'title','revisions','custom-fields','page-attributes'
			)
		));
		register_post_type('team',array(
			'labels' =>array(
				'name'       =>'Team',
				'menu_name'  =>'Team menu',
				'all_items'  =>'All Team  Member',
				'add_new'    =>'Add new Team Member',
				'add_new_item'=>'Add new Team Member'
			),
			'public'  =>true,
			'supports'=>array(
				'title','revisions','page-attributes','thumbnail'
			)
		));
	}
	add_action('init','office_master_custom_post');

	include_once('inc/cmb2_custom.php');
	require_once('inc/redux-framework-master/redux-framework.php');
	require_once('inc/office_master_redux_option.php');
?>